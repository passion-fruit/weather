//
//  TemperatureChartView.swift
//  Weather
//
//  Created by Karl Grogan on 06/09/2017.
//  Copyright © 2017 Karl Grogan. All rights reserved.
//

import UIKit
import Charts

class TemperatureChartView: UIView {
    var chart: LineChartView = LineChartView()
    var dataSet: LineChartDataSet = LineChartDataSet()
    var data: LineChartData = LineChartData()
    var chartTitle: UILabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor(red: 21/255, green: 30/255, blue: 41/255, alpha: 1)
        addSubview(chart)
        addSubview(chartTitle)
        setupChartTitle()
        setupChart()
        setupLayoutConstrainints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupChartTitle() {
        chartTitle.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        chartTitle.text = "Temperature (°C)"
        chartTitle.textAlignment = .center
        chartTitle.numberOfLines = 1
        chartTitle.textColor = UIColor.white
        chartTitle.font = UIFont.systemFont(ofSize: 12)
    }
    
    func setupChart() {
         // LineChartView setup.
        chart.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height) 
        chart.xAxis.drawGridLinesEnabled = false
        chart.xAxis.drawAxisLineEnabled = false // This removes the x-axis line at the top.
        chart.xAxis.labelFont = UIFont.systemFont(ofSize: 12)
        chart.xAxis.granularity = 1
        chart.leftAxis.enabled = false
        chart.rightAxis.enabled = false
        chart.drawBordersEnabled = false
        chart.minOffset = 0
        chart.legend.enabled = false
        chart.setScaleEnabled(false)
        chart.animate(xAxisDuration: 0.5, yAxisDuration: 0.5, easingOption: .easeInSine)
        chart.xAxis.labelTextColor = UIColor(red: 151/255, green: 156/255, blue: 164/255, alpha: 0.5)
        chart.xAxis.labelPosition = .bottom        
        chart.setExtraOffsets(left: 0, top: 0, right: 0, bottom: 10) // Adds some padding below the x axis labels.
        chart.chartDescription?.enabled = false        
              
        // These two lines below make some extra spacing some how to pad the line chart points
        // so the line up with the bar chart below it.
        // The bar chart has the same values.
        // Extra spacing for `axisMinimum` to be added to automatically calculated `axisMinimum`
        // https://github.com/danielgindi/Charts/issues/46
        chart.xAxis.spaceMin = 0.5
        chart.xAxis.spaceMax = 0.5
        
        
        /*
         * ChartDataEntry setup
         * These are the data points for the chart.
         * We create an array of 50 data points with a value of 0 by default
         */
        var lineChartEntries = [ChartDataEntry]()
        for i in 0..<50 {
            // Create a ChartDataEntry and append it to array of data objects.
            lineChartEntries.append(ChartDataEntry(x: Double(i), y: 0.0))
        }
        
        
        /*
         * LineChartDataSet setup
         * These are the data points for the chart.
         * We create an array of 50 data points with a value of 0 by default
         */
        dataSet = LineChartDataSet(values: lineChartEntries, label: "Temperature")
        dataSet.setColor(UIColor(red: 238/255, green: 198/255, blue: 67/255, alpha: 1))
        dataSet.setCircleColor(UIColor(red: 238/255, green: 198/255, blue: 67/255, alpha: 1))
        dataSet.drawCircleHoleEnabled = false
        dataSet.lineWidth = 2.0
        dataSet.circleRadius = 7.0
        dataSet.valueTextColor = UIColor.white // This is the color of the value over the data point.
        dataSet.valueFont = UIFont.systemFont(ofSize: 12)
        
        
        // The two lines below disable the crosshairs when you select a point.
        dataSet.drawHorizontalHighlightIndicatorEnabled = false
        dataSet.drawVerticalHighlightIndicatorEnabled = false
        
        /*
         * LineChartData setup
         * We add the dataSet to the data object.
         */
        data.addDataSet(dataSet)
        
        
        // Assign our data object for the chart view.
        chart.data = data
        
        // This has to be done here after the data has been assigned for some reason - because stackoverflow.
        chart.setVisibleXRangeMaximum(8.0)        
    }
    
    func setupLayoutConstrainints() {
        chartTitle.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: chartTitle, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 10))
        addConstraint(NSLayoutConstraint(item: chartTitle, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 10))
        addConstraint(NSLayoutConstraint(item: chartTitle, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0))
        //        left out the height value so it can by dynamic i guess
        //        addConstraint(NSLayoutConstraint(item: chartTitle, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 0.1, constant: 0))
        
        chart.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: chart, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: chart, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: chart, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: chart, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 0))
        print(chart.layoutGuides)
    }
}
