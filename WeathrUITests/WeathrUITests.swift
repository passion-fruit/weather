//
//  WeathrUITests.swift
//  WeathrUITests
//
//  Created by Karl Grogan on 01/09/2017.
//  Copyright © 2017 Karl Grogan. All rights reserved.
//

import XCTest

class WeathrUITests: XCTestCase {
    var app = XCUIApplication()
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        setupSnapshot(app)
        app.launch()
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        var handled = false
        var appeared = false
        
        let token = addUIInterruptionMonitor(withDescription: "Location") { (alert) -> Bool in
            appeared = true
            let allow = alert.buttons["Allow"]
            if allow.exists {
                allow.tap()
                handled = true
                return true
            }
            
            return false
        }

//        let nextGame = self.app.navigationBars.matching(identifier: "Weathr - City of Westminster").element
//        let exists = NSPredicate(format: "exists == true")
//        expectation(for: exists, evaluatedWith: nextGame, handler: nil)
//        // Interruption won't happen without some kind of action.
//        app.tap()
//
//        waitForExpectations(timeout: 60) { (error) in
//            if error != nil { XCTFail() }
            sleep(6)
            snapshot("01Weather")
//        }

//        XCTAssert(nextGame.exists)
//        removeUIInterruptionMonitor(token)
    }
    
}
